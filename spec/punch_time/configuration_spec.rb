# frozen_string_literal: true

RSpec.describe PunchTime::Configuration do
  let(:wrong_config) do
    proc do |config|
      config.shift_in_time     = 'aaa'
      config.shift_out_time    = 'aaa'
      config.breaks = [
        {
          start_time: 'aaa',
          end_time: 'aaa'
        }
      ]
      config.night = {
        start_time: 'aaa',
        end_time: 'aaa'
      }
    end
  end

  subject { -> { PunchTime.configure(&wrong_config) } }
  it { is_expected.to raise_error ArgumentError }
end
