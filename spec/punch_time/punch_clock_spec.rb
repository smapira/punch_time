# frozen_string_literal: true

RSpec.describe PunchTime do
  describe 'has breaks' do
    before do
      PunchTime.configure do |config|
        config.shift_in_time = Time.parse('10:00')
        config.shift_out_time = Time.parse('19:00')
        config.breaks = [
          {
            start_time: Time.parse('12:00'),
            end_time: Time.parse('13:00')
          }
        ]
      end
    end

    subject { -> { PunchTime.punch('aaa', DateTime.parse('20200101 19:00')) } }
    it { is_expected.to raise_error ArgumentError }

    it 'has a version number' do
      expect(PunchTime::VERSION).not_to be nil
    end

    it 'calculates sum tardy' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 19:00'))
      expect(PunchTime.sum_tardy.minutes).to eq 10
    end

    it 'calculates sum tardy after break' do
      PunchTime.punch(DateTime.parse('20200101 13:10'), DateTime.parse('20200101 19:00'))
      expect(PunchTime.sum_tardy.minutes).to eq 190
    end

    it 'has tardy' do
      PunchTime.punch(DateTime.parse('20200101 13:10'), DateTime.parse('20200101 19:00'))
      expect(PunchTime.tardy?).to eq true
    end

    it 'calculates sum work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 19:00'))
      expect(PunchTime.sum_work.hours).to eq 8
    end

    it 'calculates sum work early withdrawal' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 12:30'))
      expect(PunchTime.sum_work.hours).to eq 2
    end

    it 'calculates sum over work 20:00' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 20:00'))
      expect(PunchTime.sum_over_work.hours).to eq 1
    end

    it 'calculates sum over work 23:00' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 23:00'))
      expect(PunchTime.sum_over_work.hours).to eq 3
    end

    it 'calculates sum over work after day' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.sum_over_work.hours).to eq 22
    end

    it 'has over work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.overtime_work?).to eq true
    end

    it 'calculates sum night work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 23:35'))
      expect(PunchTime.sum_night_work.minutes).to eq 95
    end

    it 'calculates sum night work after day' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.sum_night_work.hours).to eq 14
    end

    it 'has over work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.night_overtime_work?).to eq true
    end
  end

  describe 'has not breaks' do
    before do
      PunchTime.configure do |config|
        config.shift_in_time = Time.parse('10:00')
        config.shift_out_time = Time.parse('19:00')
        config.breaks = []
      end
    end

    it 'calculates sum work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 19:00'))
      expect(PunchTime.sum_work.hours).to eq 9
    end

    it 'calculates sum work early withdrawal' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 12:30'))
      expect(PunchTime.sum_work.hours).to eq 2
    end

    it 'calculates sum over work 20:00' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 20:00'))
      expect(PunchTime.sum_over_work.hours).to eq 1
    end

    it 'calculates sum over work 23:00' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 23:00'))
      expect(PunchTime.sum_over_work.hours).to eq 3
    end

    it 'calculates sum over work after day' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.sum_over_work.hours).to eq 22
    end

    it 'has over work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.overtime_work?).to eq true
    end

    it 'calculates sum night work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 23:35'))
      expect(PunchTime.sum_night_work.minutes).to eq 95
    end

    it 'calculates sum night work after day' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.sum_night_work.hours).to eq 14
    end

    it 'has over work' do
      PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200103 07:00'))
      expect(PunchTime.night_overtime_work?).to eq true
    end
  end
end
