# punch_time

![Ruby](https://img.shields.io/badge/Ruby-CC342D?style=for-the-badge&logo=ruby&logoColor=white)
[![Gem Version](https://badge.fury.io/rb/punch_time.svg)](https://badge.fury.io/rb/punch_time)
![](https://ruby-gem-downloads-badge.herokuapp.com/punch_time)
[![Ruby Style Guide](https://img.shields.io/badge/code_style-rubocop-brightgreen.svg)](https://github.com/rubocop-hq/rubocop)
[![CircleCI](https://circleci.com/bb/smapira/punch_time.svg?style=svg)](https://circleci.com/bb/smapira/punch_time)

![logo](https://blog.routeflags.com/wp-content/uploads/2022/05/punch-time.jpg)

Work time calculations using punch clock.

## See It Work

[![asciicast](https://asciinema.org/a/489536.svg)](https://asciinema.org/a/489536)

## Features

Calculates a work, a tardy, a overtime work and a night work times.

## Motivation

Proud to present our newest line of gem, the back office calculations series. As you know, the modern human resource calculations has been needing a large variety of case. This gem include simplest calculator for overtime work, night work and tardy time.
If you would kindly accept the deliverable, this gem willing to calculation.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'punch_time'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install punch_time


## Configuration

```ruby
PunchTime.configure do |config|
config.shift_in_time = Time.parse('10:00')
config.shift_out_time = Time.parse('19:00')
config.breaks = [
    {
    start_time: Time.parse('12:00'),
    end_time: Time.parse('13:00')
    }
]
config.night = {
    start_time: Time.parse('22:00'),
    end_time: Time.parse('05:00')
}
config.offset = '+0000'
end
```

## Usage

```ruby
# Set the your time record
PunchTime.punch(DateTime.parse('20200101 10:10'), DateTime.parse('20200101 19:00'))

# Calculations can be performed in minutes, hours, or days
p PunchTime.sum_work.hours
# => 8

p PunchTime.sum_work.minutes
# => 480

# When need calculate holiday work time (use with business_time)

require 'business_time'

sum_works = []
Date.parse('20200101').upto(Date.parse('20200105')) do |x|
  PunchTime.punch(DateTime.parse(x.to_s + ' 10:10'), DateTime.parse(x.to_s + ' 19:00'))
  sum_works.append(PunchTime.sum_work.hours) unless x.workday?
end
p sum_works.inject(:+).to_i
# => 16

# Other methods
PunchTime.sum_work
PunchTime.sum_tardy
PunchTime.sum_over_work
PunchTime.sum_night_work
PunchTime.tardy?
PunchTime.overtime_work?
PunchTime.night_overtime_work?
```

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/smapira/punch_time. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Changelog

punch_time's changelog is available [here](https://bitbucket.org/smapira/punch_time/blob/master/CHANGELOG.md.md).

## Code of Conduct

Everyone interacting in the PunchClock project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/smapira/punch_time/blob/master/CODE_OF_CONDUCT.md).

## You may enjoy owning other libraries and my company.

* [routeflagsinc / jma — Bitbucket](https://bitbucket.org/routeflagsinc/jma/src/main/)
- for the Japan Meteorological Agency (気象庁) for searching new information simplifies.
* [smapira / punch_time — Bitbucket](https://bitbucket.org/smapira/punch_time/src/master/)
- Work time calculations using punch clock.
* [smapira / rspec-css — Bitbucket](https://bitbucket.org/smapira/rspec-css/src/master/)
- Record your test suite's computed CSS during future test runs for deterministic and accurate tests.
* [routeflags/timeline_rails_helper: The TimelineRailsHelper provides a timeline_molecules_tag helper to draw a vertical time line usable with vanilla CSS.](https://github.com/routeflags/timeline_rails_helper)
* [routeflags/acts_as_tree_diagram: ActsAsTreeDiagram extends ActsAsTree to add simple function for draw tree diagram with html.](https://github.com/routeflags/acts_as_tree_diagram)
* [株式会社旗指物](https://blog.routeflags.com/)

## Аcknowledgments

- [biz](https://github.com/zendesk/biz)
- [business_time](https://github.com/bokmann/business_time)
- [Download Best Friends Comic Font OTF, TTF](https://bestoffont.com/en/best-friends-comic-font)
- [Xu Haiwei (@mrsunburnt) | Unsplash Photo Community ](https://bestoffont.com/en/best-friends-comic-font)

