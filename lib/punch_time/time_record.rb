# frozen_string_literal: true

require 'active_model'
require 'active_support/all'
require 'forwardable'
require 'date'
require 'time'

module PunchTime
  class TimeRecord
    include ActiveModel::Validations
    extend Forwardable

    attr_accessor :start_time
    attr_accessor :end_time

    validates :start_time, presence: true
    validates :end_time, presence: true
    validate :no_format

    MINUTE_SECONDS = 60
    HOUR_MINUTES   = 60
    DAY_HOURS      = 24

    Scale = Struct.new(:rational, :days, :hours, :minutes, :seconds)

    # @param [Hash] config
    def initialize(&config)
      @configuration = Configuration.new(&config)
    end

    # @param [DateTime] start_time
    # @param [DateTime] end_time
    def punch(start_time, end_time)
      self.start_time = start_time
      self.end_time = end_time
      raise ArgumentError, errors.messages unless valid?
    end

    # @return [Scale]
    def sum_night_work
      night_works = []
      night_start_time = @configuration.night[:start_time].strftime('%H:%M:%S')
      night_end_time = @configuration.night[:end_time].strftime('%H:%M:%S')
      start_time.to_date.upto(end_time.to_date) do |x|
        night_start = DateTime.parse("#{x.strftime('%Y-%m-%d')} #{night_start_time}")
                              .change(offset: @configuration.offset)
        night_end = DateTime.parse("#{x.next_day.strftime('%Y-%m-%d')} #{night_end_time}")
                            .change(offset: @configuration.offset)
        if end_time > night_end
          night_works.append(night_end - night_start)
        elsif (end_time - night_start).positive?
          night_works.append(end_time - night_start)
        end
      end
      night_work = night_works.inject(:+)
      convert_humanize(night_work)
    end

    # @return [Scale]
    def sum_over_work
      shift_out_time = @configuration.shift_out_time.strftime('%H:%M:%S')
      over_work = end_time -
                  DateTime.parse("#{start_time.strftime('%Y-%m-%d')} #{shift_out_time}")
                          .change(offset: @configuration.offset) -
                  sum_night_work.rational

      convert_humanize(over_work)
    end

    # @return [Scale]
    def sum_work
      shift_in_time = @configuration.shift_in_time.strftime('%H:%M:%S')
      shift_in = DateTime.parse("#{start_time.strftime('%Y-%m-%d')} #{shift_in_time}")
                         .change(offset: @configuration.offset)
      work = end_time - shift_in - sum_break
      convert_humanize(work)
    end

    # @return [Scale]
    def sum_tardy
      shift_in_time = @configuration.shift_in_time.strftime('%H:%M:%S')
      tardy = start_time -
              DateTime.parse("#{start_time.strftime('%Y-%m-%d')} #{shift_in_time}")
                      .change(offset: @configuration.offset)
      convert_humanize(tardy)
    end

    # @return [Boolean]
    def tardy?
      shift_in_time = @configuration.shift_in_time.strftime('%H:%M:%S')
      start_time >
        DateTime.parse("#{start_time.strftime('%Y-%m-%d')} #{shift_in_time}")
                .change(offset: @configuration.offset)
    end

    # @return [Boolean]
    def overtime_work?
      shift_out_time = @configuration.shift_out_time.strftime('%H:%M:%S')
      end_time > DateTime.parse("#{start_time.strftime('%Y-%m-%d')} #{shift_out_time}")
                         .change(offset: @configuration.offset)
    end

    # @return [Boolean]
    def night_overtime_work?
      night_start_time = @configuration.night[:start_time].strftime('%H:%M:%S')
      end_time >
        DateTime.parse("#{start_time.strftime('%Y-%m-%d')} #{night_start_time}")
                .change(offset: @configuration.offset)
    end

    protected

    attr_reader :configuration

    private

    # @return [Rational]
    def sum_break
      start_day = start_time.strftime('%Y-%m-%d')
      breaks = @configuration
               .breaks
               .select do |x|
                 end_time >
                   DateTime.parse("#{start_day} #{x[:end_time].strftime('%H:%M:%S')}")
                           .change(offset: @configuration.offset)
               end.select do |x|
        DateTime.parse("#{start_day} #{x[:start_time].strftime('%H:%M:%S')}")
                .change(offset: @configuration.offset) >
          start_time
      end
      return Rational(0) if breaks.blank?

      breaks.map { |x| to_datetime(x[:end_time]) - to_datetime(x[:start_time]) }
            .inject { :+ }
    end

    def no_format
      errors.add(:start_time, 'Allow only DateTime format') unless start_time.is_a?(DateTime)
      errors.add(:end_time, 'Allow only DateTime format') unless end_time.is_a?(DateTime)
    end

    # @return [Struct]
    def convert_humanize(value)
      value ||= Rational(0)
      value = Rational(0) if value.negative?
      Scale.new(value,
                value.to_i,
                (value * DAY_HOURS).to_i,
                (value * DAY_HOURS * HOUR_MINUTES).to_i,
                (value * DAY_HOURS * HOUR_MINUTES * MINUTE_SECONDS).to_i)
    end

    # @see https://stackoverflow.com/questions/279769/convert-to-from-datetime-and-time-in-ruby
    def to_datetime(value)
      # Convert seconds + microseconds into a fractional number of seconds
      seconds = value.sec + Rational(value.usec, 10**6)

      # Convert a UTC offset measured in minutes to one measured in a
      # fraction of a day.
      offset = Rational(value.utc_offset, 60 * 60 * 24)
      DateTime.new(value.year, value.month, value.day, value.hour, value.min, seconds, offset)
    end
  end
end
