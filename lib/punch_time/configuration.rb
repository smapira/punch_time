# frozen_string_literal: true

require 'date'
require 'time'
require 'active_model'

module PunchTime
  class Configuration
    include ActiveModel::Validations

    attr_accessor :raw
    attr_accessor :shift_in_time
    attr_accessor :shift_out_time
    attr_accessor :night
    attr_accessor :breaks
    attr_accessor :offset

    validates :shift_in_time, presence: true
    validates :shift_out_time, presence: true
    validates :night, presence: true
    validate :no_format

    def initialize
      self.raw = Raw.new

      yield raw if block_given?

      self.shift_in_time ||= raw.shift_in_time
      self.shift_out_time ||= raw.shift_out_time
      self.night ||= raw.night
      self.breaks ||= raw.breaks
      self.offset ||= raw.offset

      raise ArgumentError, errors.messages unless valid?

      raw.freeze
    end

    Raw = Struct.new(:shift_in_time, :shift_out_time, :night,
                     :breaks, :offset) do
      module Default
        BREAKS = [
          {
            start_time: Time.parse('12:00'),
            end_time: Time.parse('13:00')
          }
        ].freeze

        NIGHT = {
          start_time: Time.parse('22:00'),
          end_time: Time.parse('05:00')
        }.freeze

        START_TIME = Time.parse('10:00').freeze
        END_TIME = Time.parse('19:00').freeze
        OFFSET = '+0000'
      end

      def initialize(*)
        super

        self.shift_in_time     ||= Default::START_TIME
        self.shift_out_time    ||= Default::END_TIME
        self.night ||= Default::NIGHT
        self.breaks ||= Default::BREAKS
        self.offset ||= Default::OFFSET
      end
    end

    private_constant :Raw,
                     :Default

    private

    def no_format
      %i[shift_in_time shift_out_time].map do |x|
        errors.add(x, 'Allow only Time format') unless public_send(x).is_a?(Time)
      end
      errors.add(:night, 'Allow only Time format') unless night[:start_time].is_a?(Time)
      errors.add(:night, 'Allow only Time format') unless night[:end_time].is_a?(Time)
      errors.add(:offset, 'Allow only String format') unless offset.is_a?(String)
      breaks.each do |x|
        unless x[:start_time].blank? || x[:start_time].is_a?(Time)
          errors.add(:breaks,
                     'Allow only Time format')
        end
        unless x[:end_time].blank? || x[:end_time].is_a?(Time)
          errors.add(:breaks,
                     'Allow only Time format')
        end
      end
    end
  end
end
