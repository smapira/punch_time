# frozen_string_literal: true

module PunchTime
  class Error < StandardError
    Configuration = Class.new(self)
  end
end
