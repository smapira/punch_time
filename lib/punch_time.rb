# frozen_string_literal: true

require 'punch_time/version'
require 'punch_time/configuration'
require 'punch_time/time_record'
require 'punch_time/error'
require 'active_support/core_ext/module'
require 'forwardable'

module PunchTime
  class << self
    extend Forwardable

    def configure(&config)
      Thread.current[:time_record] = TimeRecord.new(&config)
    end

    delegate %i[
      punch
      sum_tardy
      sum_work
      sum_over_work
      sum_night_work
      tardy?
      overtime_work?
      night_overtime_work?
    ] => :time_record

    private

    def time_record
      Thread.current[:time_record] ||
        raise(Error::Configuration, "#{name} not configured")
    end
  end
end
